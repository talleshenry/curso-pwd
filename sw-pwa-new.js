(function () {
	'use strict'

	var CACHE_SHELL = 'pwa-news-shell-v2';
	var FILES_SHELL = [
		'/css/main.css',
		'/js/api.js',
		'/library/jquery-3.3.1.min.js',
		'/library/moment.min.js'
	];

	self.addEventListener('install', function (event){
		event.waitUntil(
			self.caches.open(CACHE_SHELL)
			.then(function(cache) {
				console.log('installed');
				return cache.addAll(FILES_SHELL);
			})
		);

	});

	self.addEventListener('fetch', function(event) {
		event.respondWith(
			caches.match(event.request)
				.then(function(response){
					if (response) {
						return response;
					}
					return fetch(event.request);
				}
			)
		);			
	});	
	self.addEventListener('activate', function(event) {

		var cacheWhitelist = ['pwa-news-shell-v2'];
	  
		event.waitUntil(
		  caches.keys().then(function(cacheNames) {
			return Promise.all(
			  cacheNames.map(function(cacheName) {
				if (cacheWhitelist.indexOf(cacheName) === -1) {
				  return caches.delete(cacheName);
				}
			  })
			);
		  })
		);
	  });		
})();